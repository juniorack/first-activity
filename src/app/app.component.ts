import { Component } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'Premier TP';
  posts: any;
  post1 = new Post('Mon premier Post', 'Voici le contenu du 1er post.Si vous lisez ces lignes, c\'est que nous avons au moins deux choses en commun :\n' +
    'l\'informatique vous intéresse et vous avez envie d\'apprendre à programmer.' +
    'Pour moi, tout a commencé sur un site maintenant très connu : le Site du Zéro. Étant\n' +
    'débutant et cherchant à tout prix des cours adaptés à mon niveau, je suis naturellement\n' +
    'tombé amoureux de ce site qui propose des cours d\'informatique accessibles au plus\n' +
    'grand nombre.' );
  post2 = new Post('Mon deuxième Post', 'Voici le contenu du 2ème post.Si vous lisez ces lignes, c\'est que nous avons au moins deux choses en commun :\n' +
    'l\'informatique vous intéresse et vous avez envie d\'apprendre à programmer.' +
    'Pour moi, tout a commencé sur un site maintenant très connu : le Site du Zéro. Étant\n' +
    'débutant et cherchant à tout prix des cours adaptés à mon niveau, je suis naturellement\n' +
    'tombé amoureux de ce site qui propose des cours d\'informatique accessibles au plus\n' +
    'grand nombre.')
  post3 = new Post('Mon troisième Post', 'Voici le contenu du 3ème post. Si vous lisez ces lignes, c\'est que nous avons au moins deux choses en commun :\n' +
    'l\'informatique vous intéresse et vous avez envie d\'apprendre à programmer.' +
    'Pour moi, tout a commencé sur un site maintenant très connu : le Site du Zéro. Étant\n' +
    'débutant et cherchant à tout prix des cours adaptés à mon niveau, je suis naturellement\n' +
    'tombé amoureux de ce site qui propose des cours d\'informatique accessibles au plus\n' +
    'grand nombre.')
  constructor() {
    this.posts = [
      this.post1,
      this.post2,
      this.post3
    ];
  }
}

export class Post {
  private _title: string;
  private _content: string;
  private _loveit: number;
  private _created_at: Date;
  constructor(title: string, content: string) {
    this._created_at = new Date();
    this._loveit = 0;
    this._content = content;
    this._title = title;
  }
}
