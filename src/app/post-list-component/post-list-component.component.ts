import {Component, Input, OnInit} from '@angular/core';

@Component({
  selector: 'app-post-list-component',
  templateUrl: './post-list-component.component.html',
  styleUrls: ['./post-list-component.component.css']
})
export class PostListComponentComponent implements OnInit {

  @Input() ptitle: string;
  @Input() pcontent: string;
  @Input() plastupdate: Date;
  constructor() { }

  ngOnInit() {
  }

}
