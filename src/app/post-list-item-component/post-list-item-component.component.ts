import {Component, Input, OnInit} from '@angular/core';

@Component({
  selector: 'app-post-list-item-component',
  templateUrl: './post-list-item-component.component.html',
  styleUrls: ['./post-list-item-component.component.css']
})
export class PostListItemComponentComponent implements OnInit {
  @Input() title: string;
  @Input() content: string;
  @Input() lastupdate: Date;
  loveit = 0;
  constructor() { }

  ngOnInit() {
  }
  onlove() {
    this.loveit++;
  }
  ondont() {
    this.loveit--;
  }
}
